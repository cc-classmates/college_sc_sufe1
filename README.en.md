# 西南财经大学校友小程序设计方案

#### Description
西南财经大学校友小程序致力于：

定期组织形式多样的大小型活动，不断提升活动品质，扩大品牌效应。大型的返校活动、校友的新年团拜会、各地方校友分会的迎新送新活动、并依托学校组织校友进行学术交流、科研合作等活动。成功举办各种校友联谊活动，是聚集校友情感的良好载体，更能广泛联络海内外校友，一解母校相思情，二谋母校发展兴。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
